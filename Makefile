PROJECT_NAME := git-sync
PROJECT_NAMESPACE ?= f.docker

GITSYNC_SRC_TARGET = src

IMAGE_TAG := $(shell cd $(GITSYNC_SRC_TARGET) && make push-name | sed s/.*\:// )

REGISTRY ?= registry.gitlab.com
REGISTRY_IMAGE := $(REGISTRY)/$(PROJECT_NAMESPACE)/$(PROJECT_NAME)
REGISTRY_IMAGE_TAG := $(REGISTRY_IMAGE):$(IMAGE_TAG)

DOCKER_GITSYNCED_REPO ?= https://github.com/kubernetes/git-sync

all: git-sync

$(GITSYNC_SRC_TARGET)/Makefile:
	git submodule update --init --remote --merge

.PHONY: clean git-sync push push-name run version
.ONESHELL:
git-sync: $(GITSYNC_SRC_TARGET)/Makefile
	cd src
	$(MAKE) container \
		REGISTRY=$(REGISTRY) \
		IMAGE=$(REGISTRY_IMAGE)

run:
	docker run -d --rm \
		-v /tmp/src:/tmp/dest \
		$(REGISTRY_IMAGE_TAG) \
			--repo=$(DOCKER_GITSYNCED_REPO) \
			--branch=master \
			--wait=30

push:
	docker login $(REGISTRY)
	docker push $(REGISTRY_IMAGE_TAG)

push-name:
	@echo "$(REGISTRY_IMAGE_TAG)"

version:
	@echo $(IMAGE_TAG)

clean:
	@cd src
	$(MAKE) clean
