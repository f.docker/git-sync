# Git-Sync

[git-sync](https://github.com/kubernetes/git-sync) is a simple command that
pulls a git repository into a local directory.

## Dependecies

Requires [docker buildx](https://github.com/docker/buildx) comes bundled with
Docker CE starting with 19.03, but requires experimental mode to be enabled on
the Docker CLI.
To enable it, `"experimental": "enabled"` can be added to the CLI configuration
file `~/.docker/config.json`. An alternative is to set the
`DOCKER_CLI_EXPERIMENTAL=enabled` environment variable.

## build

```bash
make git-sync
```

## run

```bash
make run
```

```
spec:
  volumes:
  - name: git-sync-volume
    emptyDir: {}

  # this container syncs the repo every 1000 seconds
  containers:
  - name: git-sync
    image: k8s.gcr.io/git-sync:v3.2.1
    volumeMounts:
    - name: git-sync-volume
      mountPath: /git
    env:
    - name: GIT_SYNC_REPO
      value: <repo>
    - name: GIT_SYNC_DEST
      value: /git
    - name: GIT_SYNC_WAIT
      value: "1000"

  # this container can access the synced data in /synced
  - name: my-container
    volumeMounts:
    - name: git-sync-volume
      mountPath: /synced
```
k
